package com.esprit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.esprit.model.User;


@Repository
public interface UserRepository extends MongoRepository<User, Long>{

	User findByLogin(String login);
}