package com.esprit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.esprit.model.Role;

@Repository
public interface RoleRepository extends MongoRepository<Role, Long>{

	 Role findByRole(String role);
}