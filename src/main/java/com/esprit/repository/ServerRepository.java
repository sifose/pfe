package com.esprit.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.esprit.model.Server;

@Repository
public interface ServerRepository extends MongoRepository<Server, Long>{

	
}