package com.esprit.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "GeneralParameters")
public class GeneralParameters {
	
	
	@Transient
    public static final String SEQUENCE_NAME = "general_parameters_sequence";
	
	
	@Id
	private long id;
	
	private String c_param;
	private String param;	
	private String value;
	private int nbre_retry;

	
	public GeneralParameters() {
		super();
		// TODO Auto-generated constructor stub
	}


	public GeneralParameters(long id, String c_param, String param, String value, int nbre_retry) {
		super();
		this.id = id;
		this.c_param = c_param;
		this.param = param;
		this.value = value;
		this.nbre_retry = nbre_retry;
	}


	@Override
	public String toString() {
		return "GeneralParameters [id=" + id + ", c_param=" + c_param + ", param=" + param + ", value=" + value
				+ ", nbre_retry=" + nbre_retry + "]";
	}
	
	

}
