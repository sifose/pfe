package com.esprit.model;



import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "Job_Log")
public class JobLog {
	
	@Transient
    public static final String SEQUENCE_NAME = "joblogs_sequence";
	
	private String task;
	private String status;
	private String exception;	
	@DBRef
    private Job job;


	public JobLog() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getTask() {
		return task;
	}


	public void setTask(String task) {
		this.task = task;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getException() {
		return exception;
	}


	public void setException(String exception) {
		this.exception = exception;
	}


	public Job getJob() {
		return job;
	}


	public void setJob(Job job) {
		this.job = job;
	}


	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}


	public JobLog(String task, String status, String exception, Job job) {
		super();
		this.task = task;
		this.status = status;
		this.exception = exception;
		this.job = job;
	}


	@Override
	public String toString() {
		return "JobLog [task=" + task + ", status=" + status + ", exception=" + exception + ", job=" + job + "]";
	}

	
	
}
