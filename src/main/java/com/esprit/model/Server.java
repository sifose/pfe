package com.esprit.model;

import java.util.List;

import javax.validation.constraints.NotBlank;


import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document (collection = "Server")
public class Server {

	
	@Transient
    public static final String SEQUENCE_NAME = "servers_sequence";
	
	
	@Indexed(unique=true)

	private String codeServer;

	private String ipServer;
	
	private String Responsable;

	private String msisdn;
	@NotBlank
	private String mail;
	private String parameters;
	private String description;
	
	@DBRef
    private List<MailNotification> mailNotificatios;
	
	@DBRef
    private List<MailHistory> mailHistory;
	
	@DBRef
    private List<SMSNotification> smsNotifications;
	
	@DBRef
    private List<SMSHistory> smsHistory;
	
	
	
	public Server() {
		super();
		// TODO Auto-generated constructor stub
	}



	public String getCodeServer() {
		return codeServer;
	}



	public void setCodeServer(String codeServer) {
		this.codeServer = codeServer;
	}



	public String getIpServer() {
		return ipServer;
	}



	public void setIpServer(String ipServer) {
		this.ipServer = ipServer;
	}



	public String getResponsable() {
		return Responsable;
	}



	public void setResponsable(String responsable) {
		Responsable = responsable;
	}



	public String getMsisdn() {
		return msisdn;
	}



	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}



	public String getMail() {
		return mail;
	}



	public void setMail(String mail) {
		this.mail = mail;
	}



	public String getParameters() {
		return parameters;
	}



	public void setParameters(String parameters) {
		this.parameters = parameters;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public List<MailNotification> getMailNotificatios() {
		return mailNotificatios;
	}



	public void setMailNotificatios(List<MailNotification> mailNotificatios) {
		this.mailNotificatios = mailNotificatios;
	}



	public List<MailHistory> getMailHistory() {
		return mailHistory;
	}



	public void setMailHistory(List<MailHistory> mailHistory) {
		this.mailHistory = mailHistory;
	}



	public List<SMSNotification> getSmsNotifications() {
		return smsNotifications;
	}



	public void setSmsNotifications(List<SMSNotification> smsNotifications) {
		this.smsNotifications = smsNotifications;
	}



	public List<SMSHistory> getSmsHistory() {
		return smsHistory;
	}



	public void setSmsHistory(List<SMSHistory> smsHistory) {
		this.smsHistory = smsHistory;
	}



	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}



	public Server(@NotBlank String codeServer, @NotBlank String ipServer, String responsable, @NotBlank String msisdn,
			@NotBlank String mail, String parameters, String description, List<MailNotification> mailNotificatios,
			List<MailHistory> mailHistory, List<SMSNotification> smsNotifications, List<SMSHistory> smsHistory) {
		super();
		this.codeServer = codeServer;
		this.ipServer = ipServer;
		Responsable = responsable;
		this.msisdn = msisdn;
		this.mail = mail;
		this.parameters = parameters;
		this.description = description;
		this.mailNotificatios = mailNotificatios;
		this.mailHistory = mailHistory;
		this.smsNotifications = smsNotifications;
		this.smsHistory = smsHistory;
	}


	
	
}