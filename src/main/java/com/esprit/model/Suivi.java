package com.esprit.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;


@Document (collection = "Suivi")
public class Suivi {
	@Transient
    public static final String SEQUENCE_NAME = "suivis_sequence";
	@Id
	private long id;
	@DBRef
    private User user;
	@DBRef
    private Server serveur;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Server getServeur() {
		return serveur;
	}
	public void setServeur(Server serveur) {
		this.serveur = serveur;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	public Suivi(long id, User user, Server serveur) {
		super();
		this.id = id;
		this.user = user;
		this.serveur = serveur;
	}
	public Suivi() {
		super();
		// TODO Auto-generated constructor stub
	}

}
