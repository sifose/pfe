package com.esprit.model;


import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;



@Document (collection = "User")
 public class User {

		@Transient
	    public static final String SEQUENCE_NAME = "users_sequence";
		
		@Id
		private long id;
		@NotBlank
		@Indexed(unique=true)
		private String login;
		@NotBlank
		private String password;
		private Boolean enabled;
		private String firstName;
		private String lastName;
		private String email;
		private long tel;
		private int fFirstAccess;
		private String image;
		@DBRef
	    private Set<Role> roles;
		
		

		public User() {
			super();
			// TODO Auto-generated constructor stub
		}



		public String getLogin() {
			return login;
		}



		public void setLogin(String login) {
			this.login = login;
		}



		public String getPassword() {
			return password;
		}



		public void setPassword(String password) {
			this.password = password;
		}



		public Boolean getEnabled() {
			return enabled;
		}



		public void setEnabled(Boolean enabled) {
			this.enabled = enabled;
		}



		public String getFirstName() {
			return firstName;
		}



		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}



		public String getLastName() {
			return lastName;
		}



		public void setLastName(String lastName) {
			this.lastName = lastName;
		}



		public String getEmail() {
			return email;
		}



		public void setEmail(String email) {
			this.email = email;
		}



		public long getTel() {
			return tel;
		}



		public void setTel(long tel) {
			this.tel = tel;
		}



		public int getfFirstAccess() {
			return fFirstAccess;
		}



		public void setfFirstAccess(int fFirstAccess) {
			this.fFirstAccess = fFirstAccess;
		}



		public String getImage() {
			return image;
		}



		public void setImage(String image) {
			this.image = image;
		}



		public Set<Role> getRoles() {
			return roles;
		}



		public void setRoles(Set<Role> roles) {
			this.roles = roles;
		}



		public static String getSequenceName() {
			return SEQUENCE_NAME;
		}






		public long getId() {
			return id;
		}



		public void setId(long id) {
			this.id = id;
		}



		@Override
		public String toString() {
			return "User [id=" + id + ", login=" + login + ", password=" + password + ", enabled=" + enabled
					+ ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", tel=" + tel
					+ ", fFirstAccess=" + fFirstAccess + ", image=" + image + ", roles=" + roles + "]";
		}



		public User(long id, @NotBlank String login, @NotBlank String password, Boolean enabled, String firstName,
				String lastName, String email, long tel, int fFirstAccess, String image, Set<Role> roles) {
			super();
			this.id = id;
			this.login = login;
			this.password = password;
			this.enabled = enabled;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.tel = tel;
			this.fFirstAccess = fFirstAccess;
			this.image = image;
			this.roles = roles;
		}



		

}