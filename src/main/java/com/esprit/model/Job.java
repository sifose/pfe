package com.esprit.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "Job")
public class Job {
	
	@Transient
    public static final String SEQUENCE_NAME = "jobs_sequence";
	
	private String task;
	private String unite;
	private float frequency;
	private int fRun;
	private Date date_next_exec;
	private Date date_start_exec;
	private Date date_end_exec;
	
	public Job() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getUnite() {
		return unite;
	}

	public void setUnite(String unite) {
		this.unite = unite;
	}

	public float getFrequency() {
		return frequency;
	}

	public void setFrequency(float frequency) {
		this.frequency = frequency;
	}

	public int getfRun() {
		return fRun;
	}

	public void setfRun(int fRun) {
		this.fRun = fRun;
	}

	public Date getDate_next_exec() {
		return date_next_exec;
	}

	public void setDate_next_exec(Date date_next_exec) {
		this.date_next_exec = date_next_exec;
	}

	public Date getDate_start_exec() {
		return date_start_exec;
	}

	public void setDate_start_exec(Date date_start_exec) {
		this.date_start_exec = date_start_exec;
	}

	public Date getDate_end_exec() {
		return date_end_exec;
	}

	public void setDate_end_exec(Date date_end_exec) {
		this.date_end_exec = date_end_exec;
	}

	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}

	public Job(String task, String unite, float frequency, int fRun, Date date_next_exec, Date date_start_exec,
			Date date_end_exec) {
		super();
		this.task = task;
		this.unite = unite;
		this.frequency = frequency;
		this.fRun = fRun;
		this.date_next_exec = date_next_exec;
		this.date_start_exec = date_start_exec;
		this.date_end_exec = date_end_exec;
	}

	@Override
	public String toString() {
		return "Job [task=" + task + ", unite=" + unite + ", frequency=" + frequency + ", fRun=" + fRun
				+ ", date_next_exec=" + date_next_exec + ", date_start_exec=" + date_start_exec + ", date_end_exec="
				+ date_end_exec + "]";
	}
	
	

}
