package com.esprit.model;

import java.util.Date;



import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "SMS_History")
public class SMSHistory {
	
	@Transient
    public static final String SEQUENCE_NAME = "sms_history_sequence";
	
    private String texte;
	private Date dateInsertion;
	private Date dateEnvoi;
	private String etat;
	private int nbreRetry;
	public Date getDateInsertion() {
		return dateInsertion;
	}
	public void setDateInsertion(Date dateInsertion) {
		this.dateInsertion = dateInsertion;
	}
	public Date getDateEnvoi() {
		return dateEnvoi;
	}
	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public int getNbreRetry() {
		return nbreRetry;
	}
	public void setNbreRetry(int nbreRetry) {
		this.nbreRetry = nbreRetry;
	}
	public static String getSequenceName() {
		return SEQUENCE_NAME;
	}
	
	
	
	public String getTexte() {
		return texte;
	}
	public void setTexte(String texte) {
		this.texte = texte;
	}
	
	public SMSHistory() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SMSHistory(String texte, Date dateInsertion, Date dateEnvoi, String etat, int nbreRetry) {
		super();
		this.texte = texte;
		this.dateInsertion = dateInsertion;
		this.dateEnvoi = dateEnvoi;
		this.etat = etat;
		this.nbreRetry = nbreRetry;
	}
	@Override
	public String toString() {
		return "SMSHistory [texte=" + texte + ", dateInsertion=" + dateInsertion + ", dateEnvoi=" + dateEnvoi
				+ ", etat=" + etat + ", nbreRetry=" + nbreRetry + "]";
	}
	
	
	
	
}